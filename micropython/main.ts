enum MODEL{
    //%block="开启"
    1,
    //%block="关闭"
    0
}
enum NESPIN{
    //%block="2"
    21,
    //%block="3"
    22,
    //%block="4"
    23,
    //%block="5"
    24,
    //%block="6"
    32,
    //%block="7"
    15,
    //%block="8"
    14,
    //%block="9"
    13,
    //%block="10"
    12,
    //%block="11"
    11,
    //%block="12"
    10,
    //%block="13"
    3

}

//% color="#00BFFF" iconWidth=50 iconHeight=40
namespace NES{
    //% block="声音[VOICE]" blockType="command"
    //% VOICE.shadow="dropdown" VOICE.options="MODEL" VOICE.defl="MODEL.开启"
    export function Voice(parameter: any, block: any) {
        let voice = parameter.VOICE.code; 

        Generator.addImport("from fpioa_manager import fm");
        Generator.addImport("from Maix import GPIO");

        Generator.addCode(`fm.register(32, fm.fpioa.GPIO1)`);
        Generator.addCode(`voice_en=GPIO(GPIO.GPIO1, GPIO.OUT)`);
        Generator.addCode(`voice_en.value(${voice})`);
       


    }


    //% block="NES PS手柄初始化 CS:[CS] CLK:[CLK] MOSI:[MOSI] MISO:[MISO] 音量:[VOICE]" blockType="command"
    //% CS.shadow="dropdown"     CS.options="NESPIN" CS.defl="NESPIN.21"
    //% CLK.shadow="dropdown"    CLK.options="NESPIN" CLK.defl="NESPIN.22"
    //% MOSI.shadow="dropdown"   MOSI.options="NESPIN" MOSI.defl="NESPIN.23"
    //% MISO.shadow="dropdown"   MISO.options="NESPIN" MISO.defl="NESPIN.24"
    //% VOICE.shadow="range" VOICE.params.min=1 VOICE.params.max=10  VOICE.defl=5
    export function NesInit(parameter: any, block: any) {
        let cs = parameter.CS.code; 
        let clk = parameter.CLK.code; 
        let mosi = parameter.MOSI.code; 
        let miso = parameter.MISO.code; 
        let voice = parameter.VOICE.code; 
        
        Generator.addImport("import nes_hockel",true);

        Generator.addCode(`nes_hockel.joystick_init(${cs},${clk},${mosi},${miso},${voice})`);


    }
    //% block="NES UART键盘 初始化 音量[VOICE]" blockType="command"
    //% VOICE.shadow="range" VOICE.params.min=1 VOICE.params.max=10  VOICE.defl=5
    export function NesKeyPad(parameter: any, block: any) {
        let voice = parameter.VOICE.code; 

        Generator.addImport("import nes_hockel",true);

        Generator.addCode(`nes_hockel.keyboard_init(${voice})`);


    }
    //% block="加载NES游戏路径[ROUTE]" blockType="command"
    //% ROUTE.shadow="string" ROUTE.defl="/sd/SuperMarie.nes"
    export function NesRun(parameter: any, block: any) {
        let route = parameter.ROUTE.code; 

        Generator.addCode(`nes_hockel.load(${route})`);


    }
    //% block="运行游戏" blockType="command"
    export function NESLOOP(parameter: any, block: any) {

        Generator.addCode(`nes_hockel.loop()`);


    }
    
    
    
   }
    