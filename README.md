# NES 游戏扩展库
## 概述：

【mind+ maixduino用户库】经典的 FC 红白机 游戏模拟器， 带我们回到小时候！

![](./micropython/_images/featured.png)

## 积木

![](./micropython/_images/blocks.png)

### 声音设置

### ![](./micropython/_images/1.png)

将我们的扬声器设置为[开启/关闭]模式

### PS2手柄初始化

![](./micropython/_images/2.png)

**参数说明**

- `cs`： 如果使用 `SPI` 接口的 `PS2` 手柄， 传入 `cs` 外设编号（注意不是引脚号，需要先映射引脚）
- `mosi`： 如果使用 `SPI` 接口的 `PS2` 手柄， 传入 `mosi` 外设编号（注意不是引脚号，需要先映射引脚）
- `miso`： 如果使用 `SPI` 接口的 `PS2` 手柄， 传入 `miso` 外设编号（注意不是引脚号，需要先映射引脚）
- `clk`： 如果使用 `SPI` 接口的 `PS2` 手柄， 传入 `clk` 外设编号（注意不是引脚号，需要先映射引脚）

**快捷键：**

- `移动` ： 方向键 `<-` `^` `V` `->`
- `A` ： `□`
- `B` ： `×`
- `start` ： `START`
- `select`： `SELECT`
- `退出` ： 暂无
- `音量 -` ： `R2`
- `音量 +` ： `R1`
- `运行速度 -` ： `L1`
- `运行速度 +` ： `L2`

### 键盘初始化

![](./micropython/_images/3.png)

**快捷键：**

- `移动` ： `W A S D`
- `A` ： `J`
- `B` ： `K`
- `start` ： `M` 或者 `Enter`
- `option`： `N` 或者 `\`
- `退出` ： `ESC`
- `音量 -` ： `-`
- `音量 +` ： `=`
- `运行速度 -` ： `R`
- `运行速度 +` ： `F`

### 加载NES游戏路径

![](./micropython/_images/5.png)

在根目录下有一个<u>**nes游戏全集.rar**</u>文件，下载解压到SD卡，之后加载游戏路径就可以畅游在儿时的回忆中去了

### 运行游戏

![](./micropython/_images/4.png)

**注：此积木块要放到重复执行积木块中**



## 程序设计

### 任务一：用键盘玩NES游戏

![](./micropython/_images/8.png)

### 任务二：用PS2手柄玩NES游戏


![](./micropython/_images/example.png)

**显示效果如下：**

![](./micropython/_images/6.jpg)




# 支持列表

|主板型号|实时模式|ArduinoC|MicroPython|备注|
|-----|-----|:-----:|:-----:|-----|
|mpython|||√||



# 更新日志

V0.0.1 基础功能完成

